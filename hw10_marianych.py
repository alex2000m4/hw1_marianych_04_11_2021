# 1.Доработайте классы Point и Line из занятия. Обеспечьте передачу в атрибуты x и y класса Point только чисел,
#а в атрибуты start_point и end_point класса Line только обьектов класса Point
# 2. Реализуйте в классе Point механизм сложения таким образом, чтобы при сложении двух точек получался обьект класса Line

class Point:
    x = 0
    y = 0

    def __init__(self, new_x, new_y):
        if not isinstance(new_x,(int, float)) or not isinstance(new_y,(int, float)):
            raise TypeError
        self.x = new_x
        self.y = new_y

    def __add__(self, other):
        result = Line(Point(self.x, self.y), Point(other.x, other.y))
        return result


class Line:
    start_point = None
    end_point = None
    def __init__(self, new_start_point, new_end_point):
        if not isinstance(new_start_point,(Point)) or not isinstance(new_end_point,(Point)):
            raise TypeError
        self.start_point = new_start_point
        self.end_point = new_end_point

    @property
    def math_length(self):
        result = ((self.end_point.x - self.start_point.x) ** 2 + (self.end_point.y - self.start_point.y) ** 2) ** 0.5
        return result


point1 = Point(2, 4)
print(point1.x, point1.y)
line1 = Line(Point(2, 3), Point(4, 5))
print(line1.start_point.x, line1.start_point.y)
print(line1.end_point.x, line1.end_point.y)
point2 = Point(6, 7)
line2 = point1 + point2
print(type(line2))
print(line2.start_point.x, line2.end_point.y)
print(line2.math_length)
