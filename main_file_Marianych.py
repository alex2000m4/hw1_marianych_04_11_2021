#В отдельном файле (пусть будет lib.py) написать функцию, которая требует от пользователя ответить да или нет (Y/N)
#и возвращает True/False в зависимости от того, что он ввел. В основном файле (пусть будет main_file.py)
#попросить пользователя ввести с клавиатуры строку и вывести ее на экран.
# Используя импортированную из lib.py функцию спросить у пользователя, хочет ли он повторить операцию (Y/N).
# Повторять пока пользователь отвечает Y и прекратить когда пользователь скажет N.


from lib_Marianych import yes_no
def user_input():
    while True:
        user_input_str = input("Enter some line: ")
        print(user_input_str)
        if yes_no() is False:
            break
    return "Bye, Bro!)"

user_string = user_input()
print(user_string)


#Модифицируем ДЗ2. Напишите с помощью функций!.
#Помните о Single Respinsibility! Попросить ввести свой возраст (можно использовать константу или input()).
#Пользователь ввел значение возраста [year number] а на место [year string]
#нужно поставить правильный падеж существительного "год", который зависит от значения [year number].
# a - если пользователь ввел непонятные данные (ничего не ввел,
#ввел не число, неактуальный возраст и тд.) - вывести “не понимаю”
# b - если пользователю меньше 7 - вывести “Тебе [year number] [year string], где твои мама и папа?”
# c - если пользователю меньше 18 - вывести “Тебе [year number] [year string],
# а мы не продаем сигареты несовершеннолетним”
# d - если пользователю больше 65 - вывести “Вам уже [year number] [year string], вы в зоне риска”!
# e - в любом другом случае - вывести “Оденьте маску, вам же [year number] [year string]!”

def case_year():
    while True:
        try:
            user_age_input = int(input("Enter your age: "))
            if user_age_input >= 0:
                break
            else:
                print("Enter your real age!")
        except Exception:
            print("What?")
    if user_age_input in range(10,20):
        case_year_str = "лет"
    elif user_age_input == 1 or user_age_input % 10 == 1:
        case_year_str = "год"
    elif user_age_input % 10 == 2 or user_age_input % 10 == 3 or user_age_input % 10 == 4:
        case_year_str = "года"
    else:
        case_year_str = "лет"
    return user_age_input, case_year_str

def func_answer():
    user_year = case_year()
    if user_year[0] == 0:
        answer = "Введите правильный возраст!"
    elif user_year[0] < 7:
        answer = f"Тебе {user_year[0]} {user_year[1]}, где твои мама и папа?"
    elif user_year[0] >= 7 and user_year[0] < 18:
        answer = f"Тебе {user_year[0]} {user_year[1]}, мы не продаем сигареты несовершеннолетним!"
    elif user_year[0] >= 65:
        answer = f"Вам {user_year[0]} {user_year[1]}, Вы в зоне риска!"
    else:
        answer = f"Оденьте маску, вам же {user_year[0]} {user_year[1]}!"
    return answer

all_res = func_answer()
print(all_res)
