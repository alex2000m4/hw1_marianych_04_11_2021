while True:
    user_words = input("Введите строку: ")
    user_words = user_words.lower()
    user_lst = user_words.split(' ')
    user_lst1 = []
    for i in user_lst:
        if i.endswith('o') or i.endswith('о'):
            user_lst1.append(i)
    counter = len(user_lst1)
    if counter > 0:
        print("В вашей строке слов с буквой 'o' в конце: " + str(counter))
        break
    else:
        print("В вашей строке нет слов с буквой 'o' в конце")