#Есть list с данными lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишите механизм, который формирует новый list (например lst2),
# который содержит только переменные-строки, которые есть в lst1.

lst = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
new_lst = []
for i in lst:
    if type(i) == str:
        new_lst.append(i)
print(new_lst)