#1.Сформировать строку, в которой содержится информация определенном слове в строке.
# Например "The [номер] symbol in [тут слово] is [значение символа по номеру в слове]".
# Слово и номер получите с помощью input() или воспользуйтесь константой.
# Например (если слово - "Python" а символ 3) - "The 3 symbol in "Python" is t".

while True:                                            #Проверка ввода числа
    try:
        user_num = int(input("Enter number:" ))
        break
    except:
        print("Enter number!!!")

while True:                                            #Проверка ввода слова
    try:
        user_word = input("Enter word:" )
        if user_word.isalpha() == True:
            break
    except:
         pass

if user_num <= 0:                                          #Проверка ввода 0 или отрицательного числа
    print("Enter numer > 0")
else:
    try:
        print(f"The {user_num} symbol in {user_word} is " + str(user_word[user_num - 1]))
    except IndexError:
        print("Your word has fewer characters!!!")


