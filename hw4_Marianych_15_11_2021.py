#1.Есть строка произвольного содержания.
# Написать код, который найдет в строке самое короткое слово, в котором присутствуют подряд две гласные буквы.

user_str = input("Enter words and use only English: ")
user_str = user_str.lower()
user_lst = user_str.split()


let_list = ['a', 'e', 'i', 'o', 'u']

count = 0
lower_words = dict()

for word in user_lst:
    for i in word:
        if i in let_list:
            count += 1
        else:
            count = 0
        if count == 2:
            for word in user_lst:
                lower_words[len(word)] = word
try:
    lw_word = lower_words[min(lower_words)]
    print(f'The shortest word with two vowels in a row: {lw_word}')
except Exception:
    print("No words with two vowels in a row!")

#2. Есть два числа - минимальная цена и максимальная цена.
#Дан словарь продавцов и цен на какой то товар у разных продавцов:
#{ "citrus": 47.999, "istudio" 42.999, "moyo": 49.999, "royal-service": 37.245,
#"buy.ua": 38.324, "g-store": 37.166, "ipartner": 38.988,
# "sota": 37.720, "rozetka": 38.003}.
# Написать код, который найдет и выведет на экран список продавцов,
#чьи цены попадают в диапазон между нижней и верхней ценой.
dict = { "citrus": 47.999, "istudio": 42.999,
            "moyo": 49.999, "royal-service": 37.245,
            "buy.ua": 38.324, "g-store": 37.166,
            "ipartner": 38.988, "sota": 37.720,
            "rozetka": 38.003}

print(dict)

user_lower_price = float(input("Enter lower price: "))
user_higher_price = float(input("Enter higher price: "))

for key, price in dict.items():
    if price >= user_lower_price and price <= user_higher_price:
        print(key, price)




