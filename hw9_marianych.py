#Создайте класс "Транспортное средство" и отнаследуйтесь от него классами "Самолет", "Автомобиль", "Корабль".
#Продумайте и реализуйте в классе "Транспортное средство" общие атрибуты для "Самолет", "Автомобиль", "Корабль".
#В наследниках реализуйте характерные для них атрибуты и методы

class Vehicle:
    engine = 0
    fuel = 0
    number_of_seats = 0
    passenger = 0

class Airplane(Vehicle):
    liters_per_hundred_km = 3
    engine = 1000
    number_of_seats = 12
    result_fuel = 0
    def __init__(self, new_fuel, new_passenger):
        self.fuel = new_fuel
        self.passenger = new_passenger

    def remainder(self):
        if self.passenger in range(1, 13):
            self.result_fuel = self.fuel / (self.passenger * self.liters_per_hundred_km)
            return self.result_fuel
        else:
            print("Maximum passengers: 12")

    def info_plane(self):
        print(f"Airplan:'Maize', number_of_seats: {self.number_of_seats}, fuel quantity: {self.fuel}, engine power: {self.engine}")
        print(f"With {self.passenger} passengers this amount of fuel is enough for {self.remainder()} km")

maize = Airplane(450, 6)
print(maize.info_plane())

class Ship(Vehicle):
    liters_per_hundred_km = 0.2
    engine = 3
    number_of_seats = 6
    result_fuel = 0

    def __init__(self, new_fuel, new_passenger):
        self.fuel = new_fuel
        self.passenger = new_passenger

    def remainder(self):
        if self.passenger in range(1, 7):
            self.result_fuel = self.fuel / (self.passenger * self.liters_per_hundred_km)
            return self.result_fuel
        else:
            print("Maximum passengers: 6")
    def info_ship(self):
        print(f"Ship:'Powerboat', number_of_seats: {self.number_of_seats}, fuel quantity: {self.fuel}, engine power: {self.engine}")
        print(f"With {self.passenger} passengers this amount of fuel is enough for {self.remainder()} km")

general_kungurtsev = Ship(40, 6)
print(general_kungurtsev.info_ship())

class Car(Vehicle):
    liters_per_hundred_km = 8.6
    engine = 625
    number_of_seats = 5
    result_fuel = 0

    def __init__(self, new_fuel, new_passenger):
        self.fuel = new_fuel
        self.passenger = new_passenger

    def remainder(self):
        if self.passenger in range(1, 6):
            self.result_fuel = self.fuel / (self.passenger * 1.25)
            return self.result_fuel
        else:
            print("Maximum passengers: 5")
    def info_car(self):
        print(f"Car:'BMW X6', number_of_seats: {self.number_of_seats}, fuel quantity: {self.fuel}, engine power: {self.engine}")
        print(f"With {self.passenger} passengers this amount of fuel is enough for {self.remainder()} km")

bmw = Car(250, 2)
print(bmw.info_car())