#Напишите класс, отвечающий за животное. Реализуйте в классе атрибуты : количество лап, издаваемый звук, кличка.
# Реализуйте в классе метод "издать звук".
# Количество лап и звук задается при инициализации и имеет ограничения (ограничения придумайте сами).
# Кличка дается после инициализции. Создайте несколько обьектов класса, напр кошка, собака, птица, и тд.

class Animals:
    __sound = ""
    _paws = 0
    nickname = ""

    def __init__(self, new_sound, new_paws):
        self.__sound = new_sound
        self._paws = new_paws

    def animal_info(self):
        return f"The animal name: {self.nickname}, he has self {self._paws} paws, makes a sound: {self.__sound}"



cat = Animals("meow", 4)
cat.nickname = "Alfred"
dog = Animals("woof", 4)
dog.nickname = "Batista"
bird = Animals("tweet", 2)
bird.nickname = "Axe"
print(Animals.animal_info(cat))
print(Animals.animal_info(dog))
print(Animals.animal_info(bird))

