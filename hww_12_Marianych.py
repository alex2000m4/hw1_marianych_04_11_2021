# 1.Подключитесь к API НБУ ( документация тут https://bank.gov.ua/ua/open-data/api-dev ),
# получите текущий курс валют и запишите его в TXT-файл в таком формате:
# "[дата создания запроса]"
# 1) [название ввалюты 1] to UAH: [значение курса к валюте 1]
# 2) [название ввалюты 2] to UAH: [значение курса к валюте 2]
# 3) [название ввалюты 3] to UAH: [значение курса к валюте 3]
# ...
# n) [название ввалюты n] to UAH: [значение курса к валюте n]
# 2. Пользователь вводит название валюты и дату,
# программа возвращает пользователю курс гривны к этой валюте за указаную дату используя API НБУ.
#Формат ввода пользователем данных - на ваше усмотрение. Реализовать с помощью ООП!

import requests
import datetime

class Currency_Converter:
    date = '20211210'
    currency = ''

    def __init__(self, new_date, new_currency):
        self.date = new_date
        self.currency = new_currency

    def writer_txt_file(self):
        api = requests.request('GET', 'https://bank.gov.ua/NBU_Exchange/exchange?json')
        str_json = list.copy(api.json())
        currency_dict = {}
        counter = 0
        for i in str_json:
            currency_dict[i['CurrencyCodeL']] = i['Amount']

        date_cur = str(datetime.datetime.now())
        with open('Currency_today', 'w') as file:
            file.write(date_cur + '\n')
            for key, val in currency_dict.items():
                counter += 1
                file.write(f'{counter}. {key} to UAH: {val}' + '\n')

    def user_inquiry(self):
        user_date = self.date
        user_input_currency = self.currency
        api = requests.get(f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date={user_date}&json')
        str_json2 = api.json()
        currency_dict = {}
        for i in str_json2:
            currency_dict[i['cc']] = i['rate']

        for key, val in currency_dict.items():
            try:
                if key == user_input_currency:
                    print(f'{key} to UAH: {val}')
            except:
                print('Exception!')

    def user_input(self):
        user_date = str(input("Enter the date on which you would like to view the course. YYYYMMDD format: "))
        user_cur = str(input("Enter currency, example: 'USD': "))
        object = Currency_Converter(user_date, user_cur)
        return object.user_inquiry()

    def start(self):
        self.user_input()

a = Currency_Converter('20211210', 'USD')
print(a.user_inquiry())
a.start()
a.writer_txt_file()