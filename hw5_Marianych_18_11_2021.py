from random import randint
#1.Написать функцию, принимающую два аргумента. Функция должна :
#   a. - если оба аргумента относятся к числовым типам - вернуть их произведение,
#   b. - если к строкам - соединить в одну строку и вернуть,
#   c. - если первый строка, а второй нет - вернуть словарь (dict), в котором ключ - первый аргумент, значение - второй
#   d. - в любом другом случае вернуть кортеж (tuple) из аргументов


def uni_func(arg1, arg2):
    if (type(arg1) == int or type(arg1) == float) and (type(arg2) == int or type(arg2) == float):
        res_num = arg1 * arg2
        return res_num
    elif type(arg1) == str and type(arg2) == str:
        res_str = arg1 + arg2
        return res_str
    elif type(arg1) == str and type(arg2) == int:
        res_dict = {arg1, arg2}
        return res_dict
    else:
        res_tup = (arg1, arg2)
        return res_tup


res_num = uni_func(2,4.0)
print(res_num)
res_str = uni_func("Introduction ", "Python")
print(res_str)
res_dict = uni_func("My age: ", 21)
print(res_dict)
res_tup = uni_func("&", [1, 4, 6, 20])
print(res_tup)

#2.Пишем игру ) Программа выбирает из диапазона чисел (1-100)
#случайное число и предлагает пользователю его угадать.
# Пользователь вводит число. Если пользователь не угадал - предлагает пользователю угадать еще раз,
# пока он не угадает. Если угадал - спрашивает хочет ли он повторить игру (Y/N).
# Если Y - повторить игру. N - закончить

def game_random():
    while True:
        rand_num = randint(1, 100)
        user_num = input("Hello!:) Let's play a game? The rules are very simple;) Please enter a number between 1 and 100: ")
        print(rand_num)
        try:
            user_num = int(user_num)
            if user_num <= 0 or user_num > 100:
                print("Your number is out of range!")
            else:
                while True:
                    user_num_3 = input("Happens :( More? Y/N")
                    if user_num_3 == "Y" or user_num_3 == "y":
                        print(game_random())
                    elif user_num_3 == "N" or user_num_3 == "n":
                        print("Bye!;)")
                    break
        except:
            print("You didn't enter a number!")
            break
        if rand_num == user_num:
            while True:
                user_num_2 = input("Cool, you won! Let's play again? Y/N")
                if user_num_2 == "y" or user_num_2 == "Y":
                    print(game_random())
                elif user_num_2 == "n" or user_num_2 == "N":
                    print("Well, see you later!")
                    break
        else:
            print("Try again! :)")
            print(game_random())

print(game_random())


#Пользователь вводит строку произвольной длины.
# Написать функцию, которая должна вернуть словарь следующего содержания:
#   a. ключ - количество букв в слове, значение - список слов с таким количеством букв.
#   b. отдельным ключем, например "0", записать количество пробелов.
#   c. отдельным ключем, например "punctuation", записать все уникальные знаки препинания, которые есть в тексте.

def func_dict (user_str = input("Enter any line: ")):
    special_char = """"'{}[].,/\?!@#$%^&*()_:;"""
    spec_char_in_str = set()
    for spec_char in user_str:
        if spec_char in special_char:
            spec_char_in_str.add(spec_char)
    spec_char_in_str = tuple(spec_char_in_str)
    spec_char_in_str2 = {'Punctuation': spec_char_in_str}

    all_space = user_str.count(' ')
    all_space = {0: all_space}

    users_str = user_str
    for char in special_char:
        users_str = users_str.replace(char, '')
    users_str = users_str.strip(' ').lower().split()
    users_lst = []
    for long in users_str:
        users_lst.append(len(long))
    users_lst = set(users_lst)
    numb_char_in_str = {}
    users_lst1 = tuple(users_lst)
    for char_word in users_str:
        for long_char_word in users_lst1:
            if len(char_word) == long_char_word:
                if long_char_word in numb_char_in_str:
                    numb_char_in_str[long_char_word] += char_word
                else:
                    numb_char_in_str[long_char_word] = char_word
    dict_result = {}
    dict_result.update(numb_char_in_str)
    dict_result.update(spec_char_in_str2)
    dict_result.update(all_space)
    return dict_result
print(func_dict())