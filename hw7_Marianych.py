from random import randint

def func_yes_no():
    user_input = input("Want to try again? Y/N: ")
    if user_input == "Y":
        return True
    elif user_input == "N":
        return False

def user_input_number():
    while True:
        user_number = int(input("Please enter a number between 1 and 100: "))
        try:
            if user_number <= 0 or user_number > 100:
                print("Only numbers from 1 to 100!")
            else:
                break
        except Exception:
            print("Did not understand")
    return user_number

def cold_hot(random_numb, user_numbers, attemps):
    differenc = random_numb - user_numbers
    if differenc >= 10:
        print(f"Cold! Attempts left: {attemps}")
    elif differenc in range(5, 10):
        print(f"Warmer! Attempts left: {attemps}")
    elif differenc in range(1, 5):
        print(f"Hot! Attempts left: {attemps}")

def game_random():
    print("The rules of the game are very simple, you need to guess the hidden number in 3 attempts)")
    random_numb = randint(1, 100)
    print(random_numb)
    user_numbers = user_input_number()
    attemps = 3
    while random_numb != user_numbers:
        attemps -= 1
        if attemps == 0:
            print("You lose!")
            break
        cold_hot(random_numb, user_numbers, attemps)
        user_numbers = user_input_number()
    if random_numb == user_numbers:
        print("You won!")
    if func_yes_no() is False:
        print("Ok. Bye!")
    else:
        print("Let`s go!")
        game_random()
    return "See you!"

print(game_random())
