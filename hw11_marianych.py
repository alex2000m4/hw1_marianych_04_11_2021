# 1. Напишите декоратор, который бы сообщал о времени выполнения функции
# 2. Напишите переметризованный декоратор,
# который бы выводил время выполнения функции в милисекундах или секундах или минутах
# (как выводить - определяет параметр декоратора)

import time

def type_check(func):
    def _wrapper(*args, **kwargs):
        for arg in args:
            if not isinstance(arg, (str)):
                raise TypeError
        for arg in kwargs.values():
            if not isinstance(arg, (str)):
                raise TypeError

        res = func(*args, **kwargs)
        return res

    return _wrapper

@type_check
def runtime_check(type_time):
    """
    :param type_time:
        'ms' - to display time in miliseconds
        's' - to display time in seconds
        'm' - to display time in minutes
        'h' - to display time in hours
    :return: function execution time
    """
    def _wrapper_outer(func):
        def _wrapper_inner(*args, **kwargs):
            start_time = time.monotonic()
            result = func(*args, **kwargs)
            end_time = time.monotonic() - start_time
            if type_time == 'm':
                end_time = end_time / 60
                print(f"Function execution time: ---> {end_time}")
            elif type_time == 'h':
                end_time = end_time / 3600
                print(f"Function execution time: ---> {end_time}")
            elif type_time == 's':
                print(f"Function execution time: ---> {end_time}")
            elif type_time == 'ms':
                end_time = end_time * 1000
                print(f"Function execution time: ---> {end_time}")
            else:
                raise TypeError
            return result

        return _wrapper_inner

    return _wrapper_outer

@runtime_check('s')
def fibonacci(n):
    fib1 = fib2 = 1
    for i in range(2, n):
        fib1, fib2 = fib2, fib1 + fib2
        print(fib2, end=' ')

fibonacci(1000)